﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace mvvm_demo.Models
{
    public class Profile
    {
        public int ID { get; set; }

        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public List<ProfileDetail> Details { get; set; }

    }

    public class ProfileDetail
    {
        public string Detail { get; set; }
    }
}