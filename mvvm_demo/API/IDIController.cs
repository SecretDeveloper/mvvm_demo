﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace mvvm_demo.API
{
    public class IDIController : ApiController
    {
        public class Demographic
        {
            public string Label { get; set; }
            public string Value { get; set; }
            public List<string> ValueList { get; set; }
            public string CensusType { get; set; }

            public bool Required { get; set; }
            public bool Enabled { get; set; }
            public bool IsList { get; set; }

            public Demographic(bool isList)
            {
                this.Label = "Label_"+DateTime.Now.Ticks.ToString();
                this.Value = "Value";
                this.ValueList = Enumerable.Repeat("value", 10).ToList();
                this.CensusType = DateTime.Now.Millisecond.ToString();

                this.Required = true;
                this.Enabled = true;
                this.IsList = isList;
            }
        }


        // GET api/idi
        public IEnumerable<Demographic> Get()
        {
            var demographics = new List<Demographic>();
            for (int i = 0; i < 100; i++)
            {
                var isList = i%2 == 0;
                demographics.Add(new Demographic(isList));
            }
            return demographics;
        }

        // GET api/idi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/idi
        public void Post([FromBody]string value)
        {
        }

        // PUT api/idi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/idi/5
        public void Delete(int id)
        {
        }
    }
}
