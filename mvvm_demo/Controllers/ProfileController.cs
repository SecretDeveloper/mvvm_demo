﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;


namespace mvvm_demo.Controllers
{
    public class ProfileController : ApiController
    {

        private static Models.Profile _profile;        
        
        public Models.Profile Get()
        {
            if (_profile == null)
            {
                _profile = new Models.Profile
                {
                    Title = "Mr",
                    Firstname = "Bill",
                    Lastname = "Gates",
                    Address = "1 Main st, Maintown, Maine",
                    Details = new List<Models.ProfileDetail> {
                            new Models.ProfileDetail(){Detail="01 12345678"},
                            new Models.ProfileDetail(){Detail="bill@microsoft.com"},
                            new Models.ProfileDetail(){Detail="Billionaire"}   
                        }
                };
            }   

            lock (_profile)
            {
                return _profile;
            }
        }
        
        public HttpResponseMessage Post([FromBody]string value)
        {
            return Request.CreateResponse(HttpStatusCode.NotImplemented);
        }

        public Models.Profile Put(Models.Profile profile)
        {
            lock (_profile)
            {
                _profile.Address = profile.Address;
                _profile.Details = profile.Details;
                _profile.Firstname = profile.Firstname;
                _profile.Lastname = profile.Lastname;
                _profile.Title = profile.Title;               

                return _profile;
            }
        }

        
        public HttpResponseMessage Delete(int id)
        {
            return Request.CreateResponse(HttpStatusCode.NotImplemented);
        }
    }
}
