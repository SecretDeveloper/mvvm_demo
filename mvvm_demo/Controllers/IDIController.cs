﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvvm_demo.Controllers
{
    public class IDIController : Controller
    {
        //
        // GET: /IDI/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /IDI/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /IDI/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /IDI/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /IDI/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /IDI/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /IDI/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /IDI/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
