﻿
using System.Web.Helpers;
using Microsoft.AspNet.SignalR.Hubs;


namespace mvvm_demo.Hubs
{
    [HubName("profile")]
    public class ProfileHub : Hub
    {
        public void update(string profile)
        {            
            Clients.All.updateProfile(profile);
        }
    }
}