﻿
function profileViewModel() {
    var self = this;

    function detailItem(root, detail) {
        var self = this;
        self.detail = ko.observable(detail);
        self.remove = function () {
            root.details.remove(self);
        };
    }

    self.addItemTitle = ko.observable("");

    self.id = ko.observable();
    self.title = ko.observable();
    self.firstName = ko.observable();
    self.lastName = ko.observable();

    self.fullName = ko.computed(function () {
        return self.title() + ' ' + self.firstName() + ' ' + self.lastName();
    });

    self.address = ko.observable();
    self.details = ko.observableArray();

    self.addDetail = function () {
        if (self.addItemTitle().length > 0) {
            self.details.push(new detailItem(self, self.addItemTitle()));
            self.addItemTitle("");
        }
    };

    
    self.insertDetail = function (title) {
        if(title.length > 0)
            self.details.push(new detailItem(self, title));
    };

    self.sendUpdate = function (item) {
        
        $.ajax({
            url: "/api/profile/" + item.id(),
            data: ko.toJSON(item),
                
            type: "PUT",
            dataType: "JSON",
            contentType:"application/JSON; charset=utf-8",
            success: function (result) {
                
            }
        });
                
        // SignalR - send update to all clients.
        var hub = $.connection.profile;
        hub.server.update(ko.toJSON(item));
    };

}

$(function () {
    var viewModel = new profileViewModel();
    ko.applyBindings(viewModel);
    $.get("/api/profile", function (item) {
        
        mapDataToViewModel(item);
        
    }, "json");

    var prof = $.connection.profile;
    prof.client.updateProfile = function (profile) {
        mapJsonToViewModel($.parseJSON(profile));
    };

    $.connection.hub.start();


    function mapJsonToViewModel(item) {
        viewModel.id(item.id);
        viewModel.title(item.title);
        viewModel.firstName(item.firstName);
        viewModel.lastName(item.lastName);
        viewModel.address(item.address);
        viewModel.details([]);
        $.each(item.details, function (idx, detail) {

            viewModel.insertDetail(detail.detail);
        })
    }

    function mapDataToViewModel(item) {
        viewModel.id(item.ID);
        viewModel.title(item.Title);
        viewModel.firstName(item.Firstname);
        viewModel.lastName(item.Lastname);
        viewModel.address(item.Address);
        $.each(item.Details, function (idx, detail) {

            viewModel.insertDetail(detail.Detail);
        })
    }

});



